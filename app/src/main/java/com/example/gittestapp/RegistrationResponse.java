package com.example.gittestapp;

public class RegistrationResponse {

    private Boolean  success;
    private String message;

    public RegistrationResponse(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "RegistrationResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }

    //response gor following api
    //    public static final String BASE_URL = "https://reqres.in";
    //    private String id;
//    private String token;
//    private String error;
//
//
//    public RegistrationResponse(String id, String token, String error) {
//        this.id = id;
//        this.token = token;
//        this.error = error;
//    }
//
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    public void setToken(String token) {
//        this.token = token;
//    }
//
//    public String getError() {
//        return error;
//    }
//
//    public void setError(String error) {
//        this.error = error;
//    }
//
//    @Override
//    public String toString() {
//        return "RegistrationResponse{" +
//                "id='" + id + '\'' +
//                ", token='" + token + '\'' +
//                ", error='" + error + '\'' +
//                '}';
//    }


}
