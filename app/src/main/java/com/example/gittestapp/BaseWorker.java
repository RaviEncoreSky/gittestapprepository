package com.example.gittestapp;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import retrofit2.Call;
import retrofit2.Response;

public abstract class BaseWorker<T> extends Worker{
    public BaseWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        try {
            Response<T> response = null;
            response = call().execute();

            if (response.isSuccessful()) {
                onSuccess(response.body());
            }
            else{
                assert response.errorBody() != null;
                String error = response.errorBody().string();
                onFailure( response);
            }
            return Result.success();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("---ex " + ex.toString());
            onCancel();
            return Result.failure();
        }
    }

    protected abstract @NonNull
    Call<T> call();

    protected abstract void onSuccess(T view);

    protected abstract void onFailure(@NonNull final Response<T> response);

    protected abstract void onCancel();

}
