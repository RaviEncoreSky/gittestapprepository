package com.example.gittestapp;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;
import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Response;

public class LoginWorker extends BaseWorker<LoginResponse> {

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    String email, password;

    public LoginWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        email = workerParams.getInputData().getString(EMAIL);
        password = workerParams.getInputData().getString(PASSWORD);
    }

    @NonNull
    @Override
    protected Call<LoginResponse> call() {

        return  ApiTestClass.getClient().login(email, password);
    }

    @Override
    protected void onSuccess(LoginResponse loginResponse) {
//        String message = loginResponse.getMessage();
        EventBus.getDefault().post(new LoginEvent(loginResponse.getMessage(),loginResponse.getSuccess(),loginResponse.getResponse().getUser().getEmail()));
//        System.out.println("---success "+ message );

    }

    @Override
    protected void onFailure(@NonNull Response<LoginResponse> response) {

//
//        try {
//            JSONObject jsonObject = new JSONObject(response.errorBody().string());
//            String message = jsonObject.getString("message");
//
//            EventBus.getDefault().post(new LoginEvent("Login Error "+message));
//            System.out.println("---failure " + message);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        String message = response.errorBody().toString();
        EventBus.getDefault().post(new LoginEvent("Login Error \n"+message,false,null));
        System.out.println("---failure " + message);

    }

    @Override
    protected void onCancel() {
        System.out.println("---canceled " +password+ "  "+email);
        EventBus.getDefault().post(new LoginEvent("On Cancel Called",false,null));

    }
}
