package com.example.gittestapp;

public class LoginResponse {

    private LogResponse response;
    private Boolean  success;
    private String message;

    public LoginResponse(LogResponse response, Boolean success, String message) {
        this.response = response;
        this.success = success;
        this.message = message;
    }

    public LogResponse getResponse() {
        return response;
    }

    public void setResponse(LogResponse response) {
        this.response = response;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "logResponse=" + response +
                ", success=" + success +
                ", message='" + message + '\'' +
                '}';
    }


    public class LogResponse {

        private User user;

        public LogResponse(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        @Override
        public String toString() {
            return "LogResponse{" +
                    "user=" + user +
                    '}';
        }
    }

        public class User{

            public String email;
            public String role;
            public String createdAt;

            public User(String email, String role, String createdAt) {
                this.email = email;
                this.role = role;
                this.createdAt = createdAt;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            @Override
            public String toString() {
                return "User{" +
                        "email='" + email + '\'' +
                        ", role='" + role + '\'' +
                        ", createdAt='" + createdAt + '\'' +
                        '}';
            }
        }
    }

    //    private String token;
//    private String error;
//
//    public LoginResponse(String token, String error) {
//        this.token = token;
//        this.error = error;
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    public void setToken(String token) {
//        this.token = token;
//    }
//
//    public String getError() {
//        return error;
//    }
//
//    public void setError(String error) {
//        this.error = error;
//    }



//}


