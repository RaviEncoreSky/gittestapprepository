package com.example.gittestapp;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Response;

public class RegisterWorker extends BaseWorker<RegistrationResponse>{
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    String email, password;

    public RegisterWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        email = workerParams.getInputData().getString(EMAIL);
        password = workerParams.getInputData().getString(PASSWORD);
    }

    @NonNull
    @Override
    protected Call<RegistrationResponse> call() {
        return  ApiTestClass.getClient().registration(email, password);
    }

    @Override
    protected void onSuccess(RegistrationResponse registrationResponse) {

        EventBus.getDefault().post(new RegisterEvent(registrationResponse.getMessage(),registrationResponse.getSuccess()));
//        System.out.println("---success "+ registrationResponse.getMessage() );

    }

    @Override
    protected void onFailure(@NonNull Response<RegistrationResponse> response) {
        String message = response.errorBody().toString();
        EventBus.getDefault().post(new RegisterEvent(" Register Error \n"+message,false));
        System.out.println("---failure " + message);
    }

    @Override
    protected void onCancel() {
        System.out.println("---canceled "  +password+ "  "+email);
        EventBus.getDefault().post(new RegisterEvent("On Cancel Called",false));

    }
}
