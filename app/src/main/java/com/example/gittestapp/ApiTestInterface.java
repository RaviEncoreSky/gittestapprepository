package com.example.gittestapp;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiTestInterface {

    @FormUrlEncoded
    @POST("/api/registration")     // API's endpoints
    Call<RegistrationResponse> registration(
            @Field("email") String email,
            @Field("password") String password);

//    for login verification
@FormUrlEncoded
    @POST("/api/login")
Call<LoginResponse> login(
            @Field("email") String email,
            @Field("password") String password );

}
