package com.example.gittestapp;

import java.util.ArrayList;
import java.util.List;

public class RecyclerEvent {

    private final String message;
    private List<MovieResponse> movieResponseList;

    public RecyclerEvent(String message, List<MovieResponse> movieResponseList) {
        this.message = message;
        this.movieResponseList = movieResponseList;
    }

    public List<MovieResponse> getMovieResponseList() {
        return movieResponseList;
    }

    public String getMessage() {
        return message;
    }
}
