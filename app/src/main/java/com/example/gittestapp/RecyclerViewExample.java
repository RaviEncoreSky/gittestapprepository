package com.example.gittestapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.os.Bundle;
import android.os.Looper;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class RecyclerViewExample extends AppCompatActivity {
    RecyclerView rvRecyclerView;
    List<MovieResponse> moviesListResponseData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_example);
        rvRecyclerView = findViewById(R.id.rvRecyclerView);

        WorkManager.getInstance().enqueue(new OneTimeWorkRequest
                .Builder(RecyclerViewWorker.class)
                .build());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RecyclerViewExample.this);
        rvRecyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onEvent(RecyclerEvent recyclerEvent){
        Looper.prepare();
        Toast.makeText(this, recyclerEvent.getMessage(), Toast.LENGTH_SHORT).show();

        moviesListResponseData = recyclerEvent.getMovieResponseList();
        System.out.println("---size " + moviesListResponseData.size());

        AdapterMovies adapterMovies = new AdapterMovies( moviesListResponseData,this);
        rvRecyclerView.setAdapter(adapterMovies);

    }

}