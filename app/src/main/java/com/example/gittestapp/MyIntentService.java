package com.example.gittestapp;

import android.app.IntentService;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

public class MyIntentService extends IntentService {

    public MyIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

       if(intent != null) {

           String msg = intent.getStringExtra("key1");

           Toast.makeText(getApplicationContext(), "asdasd", Toast.LENGTH_SHORT).show();

           HelloEvent helloEvent = new HelloEvent(msg + " message received");

           EventBus.getDefault().post(helloEvent);
       }
       else {
           EventBus.getDefault().post(new HelloEvent("intent null"));
       }

    }
}
