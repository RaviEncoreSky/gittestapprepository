package com.example.gittestapp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MoviesApiInterface {

    @GET("/json/movies.json")
        // API's endpoints
    Call<List<MovieResponse>> getMoviesList();
}
