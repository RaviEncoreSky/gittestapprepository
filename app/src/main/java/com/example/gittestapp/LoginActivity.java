package com.example.gittestapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity {

    EditText etEmail,etPassword;
    Button btnLogin, btnRegister;
    SharedPreferences Shared_pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();


        Shared_pref = getSharedPreferences("details", MODE_PRIVATE);
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
//        Checking User Already Logged or Not
        if (Shared_pref.contains("userEmail")) {
            startActivity(intent);
            finish();
        }


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                if (email.isEmpty() || password.isEmpty()) {

                    Toast.makeText(LoginActivity.this, "Please fill all details ", Toast.LENGTH_SHORT).show();

                }
                else {
                    Data workerData = new Data.Builder()
                            .putString(LoginWorker.EMAIL, (Objects.requireNonNull(email)))
                            .putString(LoginWorker.PASSWORD, (Objects.requireNonNull(password)))
                            .build();

                    WorkManager.getInstance().enqueue(new OneTimeWorkRequest
                            .Builder(LoginWorker.class)
                            .setInputData(workerData).build());
                }
                etEmail.setText("");
                etPassword.setText("");
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    // This method will be called when a LoginEvent is posted
    @Subscribe
    public void onEvent(LoginEvent loginEvent){
        Looper.prepare();
        Toast.makeText(this, loginEvent.getMessageEvent(), Toast.LENGTH_SHORT).show();

        if(loginEvent.getSuccessEvent()){
            Intent intent = new Intent(LoginActivity.this,HomeActivity.class);

            saveUser(loginEvent.getLoggedUser()); //  Save in Shared Preference
            startActivity(intent);
            finish();
        }

    }

//    Saving data in Shared Preference
    private void saveUser(String email) {
        SharedPreferences.Editor editor = Shared_pref.edit();
        editor.putString("userEmail",email);
        editor.apply();
    }

    private void init() {

        etEmail = findViewById(R.id.etEmail);
        etPassword =findViewById(R.id.etPassword);
        btnLogin =findViewById(R.id.btnLogin);
        btnRegister =findViewById(R.id.btnRegister);
    }
}