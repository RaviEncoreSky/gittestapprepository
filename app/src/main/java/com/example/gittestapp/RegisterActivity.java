package com.example.gittestapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

public class RegisterActivity extends AppCompatActivity {
    EditText etEmail,etPassword;
    Button btnRegister, btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                if (email.isEmpty() || password.isEmpty()) {

                    Toast.makeText(RegisterActivity.this, "Please fill all details ", Toast.LENGTH_SHORT).show();

                }
                else {
                Data workerData = new Data.Builder()
                        .putString(RegisterWorker.EMAIL, (Objects.requireNonNull(email)))
                        .putString(RegisterWorker.PASSWORD,(Objects.requireNonNull(password)))
                        .build();

                WorkManager.getInstance().enqueue(new OneTimeWorkRequest
                        .Builder(RegisterWorker.class)
                        .setInputData(workerData).build());
                }
                    etEmail.setText("");
                    etPassword.setText("");
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    // This method will be called when a RegisterEvent is posted
    @Subscribe
    public void onEvent(RegisterEvent registerEvent){
        Looper.prepare();
        Toast.makeText(this, registerEvent.getMessageEvent(), Toast.LENGTH_SHORT).show();

        if(registerEvent.getSuccessEvent()){
            Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }

    private void init() {

        etEmail = findViewById(R.id.etEmail);
        etPassword =findViewById(R.id.etPassword);
        btnRegister =findViewById(R.id.btnRegister);
        btnLogin =findViewById(R.id.btnLogin);
    }
}