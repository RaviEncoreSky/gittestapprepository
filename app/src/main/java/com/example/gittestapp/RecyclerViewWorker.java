package com.example.gittestapp;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class RecyclerViewWorker extends BaseWorker<List<MovieResponse>> {
    public RecyclerViewWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    protected Call<List<MovieResponse>> call() {
        return MoviesApiClass.getClient().getMoviesList();
    }

    @Override
    protected void onSuccess(List<MovieResponse> movieResponse) {
        String message = movieResponse.toString();
        List<MovieResponse> movieResponseList = movieResponse;

        EventBus.getDefault().post(new RecyclerEvent(message,movieResponseList));
        System.out.println("---success 1"+ message );
    }

    @Override
    protected void onFailure(@NonNull Response<List<MovieResponse>> response) {
        String message1 = response.errorBody().toString();
        EventBus.getDefault().post(new RecyclerEvent(message1,null));
        System.out.println("---failure " + message1);

    }

    @Override
    protected void onCancel() {
        System.out.println("---canceled ");
        EventBus.getDefault().post(new RecyclerEvent("On Cancel Called",null));
    }


}
