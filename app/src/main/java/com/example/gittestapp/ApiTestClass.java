package com.example.gittestapp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiTestClass {
//    public static final String BASE_URL = "https://reqres.in";

    public static final String BASE_URL = "https://3.129.193.188:3005";

    private static Retrofit retrofit = null;

    public static ApiTestInterface getClient() {

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ApiTestInterface apiTestInterface = retrofit.create(ApiTestInterface.class);
        return apiTestInterface;
    }

}
