package com.example.gittestapp;

import androidx.appcompat.app.AppCompatActivity;

import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {
    Button btnLogout, btnEventBus, btnRegister, btnRecyclerView;
    TextView tvLoggedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnLogout = findViewById(R.id.btnLogout);
        btnEventBus = findViewById(R.id.btnEventBusExmple);
        btnRegister = findViewById(R.id.btnRegister);
        btnRecyclerView = findViewById(R.id.btnRecyclerViewWorker);
        tvLoggedUser = findViewById(R.id.tvLoggedUser);

        SharedPreferences newPreference = getSharedPreferences("details", MODE_PRIVATE);
        tvLoggedUser.setText("Email - " + newPreference.getString("userEmail", null));


        //This is the subclass of our WorkRequest
        final OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(MyWorker.class).build();
        //A click listener for the button inside the onClick method we will perform the work
        findViewById(R.id.btnClick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkManager.getInstance().enqueue(workRequest);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor edit = newPreference.edit();
                edit.clear();
                edit.commit();
                Intent in = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(in);
                finish();
            }
        });

        btnEventBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(HomeActivity.this,EventBusExample.class);
                startActivity(intent);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(HomeActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(HomeActivity.this, RecyclerViewExample.class);
                startActivity(intent);
            }
        });


    }

}