package com.example.gittestapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class AdapterMovies extends RecyclerView.Adapter<AdapterMovies.MyViewHolder> {
    List<MovieResponse> moviesListResponseData;
    Context mContext;

    public AdapterMovies(List<MovieResponse> moviesListResponseData, Context mContext) {
        this.moviesListResponseData = moviesListResponseData;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.movies_list_item, null);
        MyViewHolder vh = new MyViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(moviesListResponseData.get(position).getTitle());
        holder.rating.setText("Rating : "+moviesListResponseData.get(position).getRating()+" / 10");
        holder.releaseYear.setText("Year : "+moviesListResponseData.get(position).getReleaseYear());

        String strGenre = "";

        for (int i = 0;i<moviesListResponseData.get(position).getGenre().size();i++){

            strGenre = strGenre +moviesListResponseData.get(position).getGenre().get(i)+ " | ";

        }
        holder.genre.setText("Genre : "+ strGenre );

        Glide.with(mContext).load(moviesListResponseData.get(position).getImage())
                .into(holder.ivMovieImage);
    }

    @Override
    public int getItemCount() {
        return moviesListResponseData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title,rating,releaseYear,genre;
        ImageView ivMovieImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.tvMovieTitle);
            rating = itemView.findViewById(R.id.tvRating);
            releaseYear = itemView.findViewById(R.id.tvReleaseYear);
            genre = itemView.findViewById(R.id.tvGenre);
            ivMovieImage = itemView.findViewById(R.id.ivMovieImage);

        }
    }
}
