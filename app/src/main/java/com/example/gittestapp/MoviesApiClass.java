package com.example.gittestapp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesApiClass {

    private static Retrofit retrofit = null;

    public static MoviesApiInterface getClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.androidhive.info")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        //Creating object for our interface
        MoviesApiInterface apiMovies = retrofit.create(MoviesApiInterface.class);
        return apiMovies;

    }

}

