package com.example.gittestapp;

public class RegisterEvent {
    private final String messageEvent;
    private final Boolean successEvent;

    public RegisterEvent(String messageEvent, Boolean successEvent) {
        this.messageEvent = messageEvent;
        this.successEvent = successEvent;
    }

    public String getMessageEvent() {
        return messageEvent;
    }

    public Boolean getSuccessEvent() {
        return successEvent;
    }
}
