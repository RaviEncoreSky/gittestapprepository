package com.example.gittestapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class EventBusExample extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_bus_example);

        Button tvOnEvent = findViewById(R.id.tvOnEvent);

        tvOnEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(EventBusExample.this, MyIntentService.class);
//                intent.putExtra("key1","hello ravi");
//                startService(intent);

                EventBus.getDefault().post(new HelloEvent("call on EventBus on Hello Event"));
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    // This method will be called when a HelloEvent is posted
    @Subscribe
    public void onEvent(HelloEvent helloEvent){
        Toast.makeText(this, helloEvent.getMessage(), Toast.LENGTH_SHORT).show();

    }

}