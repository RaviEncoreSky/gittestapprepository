package com.example.gittestapp;

public class LoginEvent {
    private final String messageEvent;
    private final Boolean successEvent;
    private final String loggedUser;

    public LoginEvent(String messageEvent, Boolean successEvent, String loggedUser) {
        this.messageEvent = messageEvent;
        this.successEvent = successEvent;
        this.loggedUser = loggedUser;
    }

    public String getMessageEvent() {
        return messageEvent;
    }

    public Boolean getSuccessEvent() {
        return successEvent;
    }

    public String getLoggedUser() {
        return loggedUser;
    }
}
